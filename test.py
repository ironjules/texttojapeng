"""Google Cloud Text-To-Speech API  application .

Example usage:
    python textojapeng.py
"""
import os
from subprocess import call

def run_quickstart():
    # [START tts_quickstart]
    """Synthesizes speech from the input string of text or ssml.

    Note: ssml must be well-formed according to:
        https://www.w3.org/TR/speech-synthesis/
    """
    from google.cloud import texttospeech
    english_phrase="What hell are you tinking?"
    outName =english_phrase.replace(' ','_').replace(',','').replace('!','').replace('?','')  
    outName += '.mp3'
    #english_phrase += "        " 
    #japanese_phrase="何時に起きますか"
    #japanese_phrase+="                   " # white sound


    # Instantiates a client
    client = texttospeech.TextToSpeechClient()
    #English Section
    # Set the text input to be synthesized
    synthesis_input = texttospeech.types.SynthesisInput(text=english_phrase)

    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='en-US',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)

    # Select the type of audio file you want returned
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)
    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(synthesis_input, voice, audio_config)

    # The response's audio_content is binary.


    with open(outName, 'wb') as out:
        # Write the response to the output file.
        out.write(response.audio_content)

    # with open('temp.mp3', 'rb+') as filehandle:
    #     filehandle.seek(-400, os.SEEK_END)
    #     filehandle.truncate()
    #with  as out:
        # Write the response to the output file.


if __name__ == '__main__':
    run_quickstart()
