#This project work only in linux and need sox program:
#sudo apt-get install sox
#sudo apt-get install libsox-fmt-mp3
"""Google Cloud Text-To-Speech API  application .

Example usage:
    python textojapeng.py
"""
from subprocess import call

def run_quickstart():
    # [START tts_quickstart]
    """Synthesizes speech from the input string of text or ssml.

    Note: ssml must be well-formed according to:
        https://www.w3.org/TR/speech-synthesis/
    """
    from google.cloud import texttospeech
    english_phrase="What time do you wake up?"
    outName =english_phrase.replace(' ','_').replace(',','').replace('!','').replace('?','')  
    outName += '.mp3'
    #english_phrase += "        " 
    japanese_phrase="何時に起きますか"
    #japanese_phrase+="                   " # white sound


    # Instantiates a client
    client = texttospeech.TextToSpeechClient()
    #English Section
    # Set the text input to be synthesized
    synthesis_input = texttospeech.types.SynthesisInput(text=english_phrase)

    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='en-US',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)

    # Select the type of audio file you want returned
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)
    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(synthesis_input, voice, audio_config)

    # The response's audio_content is binary.


    with open("us.mp3", 'wb') as out:
        # Write the response to the output file.
        out.write(response.audio_content)
    #with  as out:
        # Write the response to the output file.
    
    #Japanese Section
    synthesis_input = texttospeech.types.SynthesisInput(text=japanese_phrase)
    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='ja-JP',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE)

    audio_config = texttospeech.types.AudioConfig(
        audio_encoding = texttospeech.enums.AudioEncoding.MP3,
        speaking_rate = 0.8 )

    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(synthesis_input, voice, audio_config)

    with open("jp.mp3", 'wb') as out:
        # Write the response to the output file.
        out.write(response.audio_content)
    # The response's audio_content is binary.
    #with open(outName, 'ab+') as out:
        # Write the response to the output file.
    call(["sox","us.mp3","-r","22050","usn.mp3"])    
    call(["sox","jp.mp3","-r","22050","jpn.mp3"])
    call(["sox","usn.mp3","jpn.mp3",outName])    
    call(["rm","jp.mp3","us.mp3","jpn.mp3","usn.mp3"])
    print('Audio content written to file :' + outName)
    # [END tts_quickstart]


if __name__ == '__main__':
    run_quickstart()
